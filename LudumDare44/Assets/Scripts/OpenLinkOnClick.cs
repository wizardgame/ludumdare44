﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class OpenLinkOnClick : MonoBehaviour {
    
    [DllImport("__Internal")]
    private static extern void OpenUrl(string str);

    [SerializeField]
    private string url;

    private void Awake() {
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    private void OnClick() {
        OpenUrl(url);
    }
}
